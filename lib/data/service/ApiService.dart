
import 'dart:convert';

import 'package:edubox_minehere/data/model/info.dart';
import 'package:http/http.dart';

class ApiService {

  String baseUrlBox = "http://api.edu.box";

  Future<Info> getInfo() async {
    try {
      Response response = await Client().get("$baseUrlBox/material/open/a5062c59-404b-43a5-bee5-b5a62d390b3a");
      if (response.statusCode == 200){
        Info info = Info.fromJson(json.decode(response.body));
        info.url = baseUrlBox;
        return info;
      } else {
        return null;
      }
    } catch(e){
      print(e);
      return null;
    }
  }
}