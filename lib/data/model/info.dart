
class Info {
  bool status;
  Data data;
  Meta meta;
  String url;

  Info({this.status, this.data, this.meta, this.url});

  Info.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  List<String> viewRoles;
  List<String> ancestors;
  List<FileIds> fileIds;
  String sId;
  String name;
  String content;
  String sluggedName;
  String publicity;
  String browserWindowTitle;
  InstitutionId institutionId;
  String parentId;
  bool open;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;
  String updatedBy;
  List<Null> sub;

  Data(
      {this.viewRoles,
        this.ancestors,
        this.fileIds,
        this.sId,
        this.name,
        this.content,
        this.sluggedName,
        this.publicity,
        this.browserWindowTitle,
        this.institutionId,
        this.parentId,
        this.open,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.updatedBy,
        this.sub});

  Data.fromJson(Map<String, dynamic> json) {
    viewRoles = json['view-roles'].cast<String>();
    ancestors = json['ancestors'].cast<String>();
    if (json['file_ids'] != null) {
      fileIds = new List<FileIds>();
      json['file_ids'].forEach((v) {
        fileIds.add(new FileIds.fromJson(v));
      });
    }
    sId = json['_id'];
    name = json['name'];
    content = json['content'];
    sluggedName = json['slugged-name'];
    publicity = json['publicity'];
    browserWindowTitle = json['browser-window-title'];
    institutionId = json['institution_id'] != null
        ? new InstitutionId.fromJson(json['institution_id'])
        : null;
    parentId = json['parent_id'];
    open = json['open'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
    updatedBy = json['updated_by'];
    /*if (json['sub'] != null) {
      sub = new List<Null>();
      json['sub'].forEach((v) {
        sub.add(new Null.fromJson(v));
      });
    }*/
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['view-roles'] = this.viewRoles;
    data['ancestors'] = this.ancestors;
    if (this.fileIds != null) {
      data['file_ids'] = this.fileIds.map((v) => v.toJson()).toList();
    }
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['content'] = this.content;
    data['slugged-name'] = this.sluggedName;
    data['publicity'] = this.publicity;
    data['browser-window-title'] = this.browserWindowTitle;
    if (this.institutionId != null) {
      data['institution_id'] = this.institutionId.toJson();
    }
    data['parent_id'] = this.parentId;
    data['open'] = this.open;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    data['updated_by'] = this.updatedBy;
    /*if (this.sub != null) {
      data['sub'] = this.sub.map((v) => v.toJson()).toList();
    }*/
    return data;
  }
}

class FileIds {
  Null deletedAt;
  Null deletedBy;
  String sId;
  FileMeta meta;
  String name;
  String url;
  String filetype;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;

  FileIds(
      {this.deletedAt,
        this.deletedBy,
        this.sId,
        this.meta,
        this.name,
        this.url,
        this.filetype,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.iV});

  FileIds.fromJson(Map<String, dynamic> json) {
    deletedAt = json['deleted_at'];
    deletedBy = json['deleted_by'];
    sId = json['_id'];
    meta = json['meta'] != null ? new FileMeta.fromJson(json['meta']) : null;
    name = json['name'];
    url = json['url'];
    filetype = json['filetype'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deleted_at'] = this.deletedAt;
    data['deleted_by'] = this.deletedBy;
    data['_id'] = this.sId;
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    data['name'] = this.name;
    data['url'] = this.url;
    data['filetype'] = this.filetype;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class FileMeta {
  String title;
  String description;
  String thumbnail;
  int duration;
  int pages;

  FileMeta(
      {this.title,
        this.description,
        this.thumbnail,
        this.duration,
        this.pages});

  FileMeta.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    thumbnail = json['thumbnail'];
    duration = json['duration'];
    pages = json['pages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['description'] = this.description;
    data['thumbnail'] = this.thumbnail;
    data['duration'] = this.duration;
    data['pages'] = this.pages;
    return data;
  }
}

class InstitutionId {
  Director director;
  Zenius zenius;
  int entrant;
  String sId;
  String name;
  String type;
  String country;
  String province;
  String city;
  String district;
  String postalCode;
  int phone;
  String email;
  String address;
  String createdBy;
  String prefix;
  String createdAt;
  String updatedAt;
  bool public;
  bool open;
  int iV;

  InstitutionId(
      {this.director,
        this.zenius,
        this.entrant,
        this.sId,
        this.name,
        this.type,
        this.country,
        this.province,
        this.city,
        this.district,
        this.postalCode,
        this.phone,
        this.email,
        this.address,
        this.createdBy,
        this.prefix,
        this.createdAt,
        this.updatedAt,
        this.public,
        this.open,
        this.iV});

  InstitutionId.fromJson(Map<String, dynamic> json) {
    director = json['director'] != null
        ? new Director.fromJson(json['director'])
        : null;
    zenius =
    json['zenius'] != null ? new Zenius.fromJson(json['zenius']) : null;
    entrant = json['entrant'];
    sId = json['_id'];
    name = json['name'];
    type = json['type'];
    country = json['country'];
    province = json['province'];
    city = json['city'];
    district = json['district'];
    postalCode = json['postal_code'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    createdBy = json['created_by'];
    prefix = json['prefix'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    public = json['public'];
    open = json['open'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.director != null) {
      data['director'] = this.director.toJson();
    }
    if (this.zenius != null) {
      data['zenius'] = this.zenius.toJson();
    }
    data['entrant'] = this.entrant;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['type'] = this.type;
    data['country'] = this.country;
    data['province'] = this.province;
    data['city'] = this.city;
    data['district'] = this.district;
    data['postal_code'] = this.postalCode;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['created_by'] = this.createdBy;
    data['prefix'] = this.prefix;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['public'] = this.public;
    data['open'] = this.open;
    data['__v'] = this.iV;
    return data;
  }
}

class Director {
  String name;
  String ktpNumber;
  String hp;
  String email;

  Director({this.name, this.ktpNumber, this.hp, this.email});

  Director.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    ktpNumber = json['ktp_number'];
    hp = json['hp'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['ktp_number'] = this.ktpNumber;
    data['hp'] = this.hp;
    data['email'] = this.email;
    return data;
  }
}

class Zenius {
  int premiumQuota;
  String status;

  Zenius({this.premiumQuota, this.status});

  Zenius.fromJson(Map<String, dynamic> json) {
    premiumQuota = json['premium_quota'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium_quota'] = this.premiumQuota;
    data['status'] = this.status;
    return data;
  }
}

class Meta {
  String message;
  int code;

  Meta({this.message, this.code});

  Meta.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}