import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:edubox_minehere/data/model/info.dart';
import 'package:edubox_minehere/ui/main/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:screen/screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

//  final List<String> imgList = [
//    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
//    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
//    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
//    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
//    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
//    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
//  ];

  List child = [];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  MainBloc _mainBloc;
  int _current = 0;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    _mainBloc = MainBloc();
    _mainBloc.dispatch(LoadInfo());

    timerData(new Duration(minutes: 30));
    Screen.keepOn(true);
    super.initState();
  }

  timerData(Duration duration){
    new Timer(duration, () {
      _mainBloc = MainBloc();
      _mainBloc.dispatch(LoadInfo());
      setState(() {

      });
      timerData(duration);
    });
  }

  @override
  Widget build(BuildContext context) {

    return BlocBuilder(
      bloc: _mainBloc,
      builder: (context, state ){
        if(state is InitialMainState){
          return Container();
        }
        if(state is LoadInfoLoading){
          return _viewLoading();
        }
        if(state is InfoLoaded){
          child = map<Widget>(
            state.info.data.fileIds,
                (index, i) {
              var data = state.info.data.fileIds[index];
              _current = index;
              return Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: "${state.info.url}/file/${data.sId}",
                        placeholder: (context, url) => Center(child: new SpinKitRing(
                          size: 25.0,
                          color: Colors.grey,
                          lineWidth: 2.0,
                        )),
                        fit: BoxFit.fill,
                        height: 1000.0,
                        width: 1000.0,
                      ),
                      /*Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              padding: const EdgeInsets.all(15.0),
                              decoration: BoxDecoration(
                                color: Colors.black26,
                                borderRadius: BorderRadius.circular(10)
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "${data.meta.title}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0
                                    ),
                                  ),
                                  Html(
                                    data: """${data.meta.description}""",
                                    defaultTextStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                      )*/
                    ],
                  ),
                ),
              );
            },
          ).toList();
          return _viewData();
        }
        if(state is LoadInfoFailed){
          return _viewError();
        }

        return _viewLoading();
      },
    );
  }

  Widget _viewData(){
    return RotationTransition(
      turns: new AlwaysStoppedAnimation(180 / 360),
      child: Center(
        child: Column(children: [
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 0),
            child: CarouselSlider(
              autoPlay: child.length > 1 ? true : false,
              autoPlayInterval: Duration(seconds: 10),
              viewportFraction: 1.0,
              aspectRatio: MediaQuery.of(context).size.aspectRatio,
              items: child,
            ),
          ),
        ]),
      ),
    );
  }

  Widget _viewLoading(){
    return Center(child: new SpinKitRing(
      size: 25.0,
      color: Colors.grey,
      lineWidth: 2.0,
    ));
  }

  Widget _viewError(){
    return RotationTransition(
      turns: new AlwaysStoppedAnimation(180 / 360),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/nosignal.png",
              height: 100,
              width: 100,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Text(
                "Koneksi terputus",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0
                ),),
            )
          ],
        ),
      ),
    );
  }
}
