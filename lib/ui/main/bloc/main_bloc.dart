import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:edubox_minehere/data/model/info.dart';
import 'package:edubox_minehere/data/service/ApiService.dart';
import './bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  @override
  MainState get initialState => InitialMainState();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if(event is LoadInfo){
      yield LoadInfoLoading();
      yield* _mapEventLoadInfo();
    }
  }

  Stream<MainState> _mapEventLoadInfo() async* {
    try {
      Info info = await ApiService().getInfo();
      if(info != null){
        yield InfoLoaded(info);
      } else {
        yield LoadInfoFailed();
      }
    } catch(e) {
      yield LoadInfoFailed();
    }
  }
}
