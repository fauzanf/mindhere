import 'package:edubox_minehere/data/model/info.dart';
import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {
  const MainState();
}

class InitialMainState extends MainState {
  @override
  List<Object> get props => [];
}

class InfoLoaded extends MainState {
  final Info info;

  InfoLoaded(this.info);

  @override
  // TODO: implement props
  List<Object> get props => [info];
}

class LoadInfoFailed extends MainState {
  @override
  // TODO: implement props
  List<Object> get props => null;

}

class LoadInfoLoading extends MainState {
  @override
  // TODO: implement props
  List<Object> get props => null;

}